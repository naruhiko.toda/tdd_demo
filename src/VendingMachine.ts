
type DrinkMenu = "water" | "cola";

class MoneyBox {
  private amount: number;
  constructor(amount: number) {
    this.amount = amount;

  }

  insert(money: number) {
    this.amount += money
  }

  getTotal() {
    return this.amount
  }

  hasEnoughMoney(price: number) {
    return this.amount >= price
  }

  remove(price: number) {
    this.amount -= price
  }

  makeEmpty() {
    this.amount = 0
  }
}

class DrinkBox {
  private stock: { water: number; cola: number };
  constructor(stock: { water: number; cola: number }) {
    this.stock = stock;
  }

  insert(drink: DrinkMenu) {
   this.stock[drink] += 1
  }

  remove(drink: DrinkMenu) {
    this.stock[drink] -= 1
  }

  hasDrink(drink: DrinkMenu) {
    return this.stock[drink] > 0;
  }

  getDrink(drink: DrinkMenu) {
    return this.stock[drink];
  }
}

export class VendingMachine {
  private moneyBox: MoneyBox = new MoneyBox(0);
  private drinkBox: DrinkBox = new DrinkBox({
    water: 0, cola: 0
  });

  addStock(drink: DrinkMenu) {
    this.drinkBox.insert(drink)
  }

  insert(money: number) {
    if (money == 10 || money == 50 || money == 100 || money == 500 || money == 1000) {
      this.moneyBox.insert(money)
      return true;
    }
    return false
  }


  getAmount() {
    return this.moneyBox.getTotal();
  }

  buy(drink: DrinkMenu) {
    let price = 0;
    if (drink === 'water') {
      price = 100;
    }
    if (drink === 'cola') {
      price = 120;
    }
    if (this.moneyBox.hasEnoughMoney(price) && this.drinkBox.hasDrink(drink)) {
      this.moneyBox.remove(price)
      this.drinkBox.remove(drink)
      return drink
    }
    return null
  }

  getStock(drink: DrinkMenu) {
    return this.drinkBox.getDrink(drink);
  }

  change() {
    let amount = this.moneyBox.getTotal();
    this.moneyBox.makeEmpty()
    return amount
  }
}
