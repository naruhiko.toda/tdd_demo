// 1 => 1
// 2 => 2
// 3 => fizz
// 6 => fizz
// 5 => buzz
// 10 => buzz
// 15 => fizzbuzz
// 30 => fizzbuzz

describe('FizzBuzz', () => {
  it('1 return 1', () => {
    expect(1).toBe(1)
  });
});
