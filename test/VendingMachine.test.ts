import {VendingMachine} from "../src/VendingMachine";

describe('VendingMachine', () => {
  describe('happy path', () => {
    test("水を購入できること", () => {
      const vm = new VendingMachine();
      vm.addStock("water")
      expect(vm.getStock("water")).toBe(1);
      let result: boolean = vm.insert(100)
      expect(result).toBe(true);
      expect(vm.getAmount()).toBe(100);
      let product = vm.buy("water")
      expect(product).toBe("water");
      expect(vm.getAmount()).toBe(0);
      expect(vm.getStock("water")).toBe(0);
      let change = vm.change();
      expect(change).toBe(0);
    });

    test("コーラを購入できること", () => {
      const vm = new VendingMachine();
      vm.addStock("cola")
      expect(vm.getStock("cola")).toBe(1);
      vm.insert(100)
      vm.insert(10)
      vm.insert(10)
      expect(vm.getAmount()).toBe(120);
      let product = vm.buy("cola")
      expect(product).toBe("cola");
      expect(vm.getAmount()).toBe(0);
      let change = vm.change();
      expect(change).toBe(0);
    });

  });

  describe('buy', () => {
    test('お金が足りていないと水は購入できないこと', () => {
      const vm = new VendingMachine();
      vm.addStock("water")
      let product = vm.buy("water")
      expect(product).toBe(null);
    });

    test('在庫が足りていないと水は購入できないこと', () => {
      const vm = new VendingMachine();
      vm.insert(100)
      expect(vm.getAmount()).toBe(100);
      let product = vm.buy("water")
      expect(product).toBe(null);
    });

    test('お金が足りていないとコーラは購入できないこと', () => {
      const vm = new VendingMachine();
      vm.addStock("cola")
      let product = vm.buy("cola")
      expect(product).toBe(null);
    });

    test('在庫が足りていないとコーラは購入できないこと', () => {
      const vm = new VendingMachine();
      vm.insert(100)
      vm.insert(10)
      vm.insert(10)
      expect(vm.getAmount()).toBe(120);
      let product = vm.buy("cola")
      expect(product).toBe(null);
    });
  });

  describe('addStock', () => {
    test('水を補充できること', () => {
      const vm = new VendingMachine();
      expect(vm.getStock("water")).toBe(0);
      vm.addStock("water")
      expect(vm.getStock("water")).toBe(1);
    });
    test('水を2回補充できること', () => {
      const vm = new VendingMachine();
      expect(vm.getStock("water")).toBe(0);
      vm.addStock("water")
      vm.addStock("water")
      expect(vm.getStock("water")).toBe(2);
    });
    test('コーラを補充できること', () => {
      const vm = new VendingMachine();
      expect(vm.getStock("cola")).toBe(0);
      vm.addStock("cola")
      expect(vm.getStock("cola")).toBe(1);
      expect(vm.getStock("water")).toBe(0);
    });
  });

  describe('insert', () => {
    test.each([
      [2000, 0, false],
      [1000, 1000, true],
      [500, 500, true],
      [100, 100, true],
      [50, 50, true],
      [10, 10, true],
      [5, 0, false],
      [1, 0, false],
    ])('正常系：%i 円を入金すると投入金額は %d 円で結果が %s であること', (insertedAmount, expectedAmount, result) => {
      const vm = new VendingMachine();
      expect(vm.getAmount()).toBe(0);
      let actualResult = vm.insert(insertedAmount);
      expect(actualResult).toBe(result);
      expect(vm.getAmount()).toBe(expectedAmount);
    });
  })

  describe('change', () => {
    test('入っているお金を全て返却すること', () => {
      const vm = new VendingMachine();
      vm.addStock("water")
      let result: boolean = vm.insert(100)
      expect(result).toBe(true);
      expect(vm.getAmount()).toBe(100);
      let change = vm.change();
      expect(change).toBe(100);
      expect(vm.getAmount()).toBe(0);
    });
  });
});
